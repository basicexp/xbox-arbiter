<?php
/**
 * Created by PhpStorm.
 * User: Dustin "BasicExp" Irvine
 * Date: 10/20/2018
 * Time: 12:26 PM
 *
 * The Xbox Arbiter API is used to fetch information from a database constructed
 * by another tool. THe stand alone tool scrapes the Xbox One store for data and
 * builds a database that this page interfaces with.
 * 
 * THis is a very simple PHP API that allows you to make calls like the following:
 * 
 *      XboxArbiter.php?index=0&fetch="PT3"&count=5
 * 
 * This call will get you five Xbox One Games, from price tier 3 (PT3) starting at an
 * index of zero
 * 
 *      XboxArbiter.php?max=PT3
 * 
 * This call will get you the total count of games in price tier 3 (PT3)
 * 
 * This page is to be called using AJAX calls to fetch a set of
 * ten Xbox One Games dependent on the GET variables sent. It will return game 
 * data in JSON format.
 * 
 *  It is designed around
 * using MySQL via PHP. If you do not have PHP installed with the MySQL
 * extension, this API will not function.
 *
 */

// Database Configuration

$servername = "localhost"; // MySQL server name here 
$username = ""; // MySQL Username here
$password = ""; // MySQL Password Here
$database = ""; // Database Name Here
$table = ""; // Table Name Here

// Helper function used to grab games based on their tier
// There is some inefficiency in using this query, consider grabbing
// all rows and manipulating them in code if performance becomes
// an issue
//
// UPDATE: Issue with web server running the query locally for unknown
// reason. Query to grab these result is 0.000006 seconds. Switching
// to grabbing all results and 
//

function GetGamesOnPrice( $connection, $table, $index,  $gamePreview, $priceFloor, $priceCeiling, $count ) {
    $temp = mysqli_query($connection,
        "SELECT * FROM " . $table . 
            " WHERE price BETWEEN " . $priceFloor . " AND " . $priceCeiling .
            " AND isGamePreview = " . $gamePreview .
            " ORDER BY arbiterScore DESC;"
    );
    
    // fetch_assoc() will grab each row, one at a time
    // and return it as an associative array, where the
    // columns are the indecies
    
    // Some inefficiency that could be worked out if performance is an issue
    // At the moment, having it work on the live server is the priority
    
    
    
    $i = 0;
    $tempXogArray = array();
    while( $xog = $temp->fetch_assoc() ) {
        if ($i >= $index && $i < ($index + $count)) {
            array_push($tempXogArray, $xog);
        }
        $i++;
    }

    return $tempXogArray;
    
}
// Check the GET variable passed in, fetch must be defined,
// count must be defined, and be a positive number
// index does not need to be defined.

if (
    isset($_GET['fetch']) &&
    isset($_GET['count']) &&
    is_numeric($_GET['count']) &&
    $_GET['count'] > 0 &&
    !isset($_GET['max']) // fetch is defined
) {

    // Ignore case
    $fetch = strtoupper($_GET['fetch']);
    $count = (int) $_GET['count'];

    if ( // Validate Price Tier
        $fetch == "PT0" || // Free
        $fetch == "PT1" || // $0.01  to $30
        $fetch == "PT2" || // $30 to $60
        $fetch == "PT3" || // $60 t $80
        $fetch == "GP" || // Game Preview
        $fetch == "ALL" // $0 tp $1000, should get everything
    ) {

        // Check for optional index
        // Initiate to -1 for the purpose of detecting
        // algorithm issues
        $index = -1;

        if ( isset($_GET["index"]) && is_numeric($_GET['index'])) {
            // We cast to an int, incase the user passes in a decimal
            // number for some reason
            $index = (int) $_GET["index"];
        } else {
            // Set the index to zero if it is otherwise not present
            // Grabs the first ten games
            $index = 0;
        }

        // Establish Connection (MySQL)
        $connection = new mysqli($servername, $username, $password, $database);
        mysqli_set_charset( $connection, 'utf8');

        if ($connection->connect_error) {
            die("DATABASE ERROR: " . $connection->connect_error);
        }

        // Build Query string based on $fetch and $index
        // values
        if ( $fetch == "PT3") {
            $result = GetGamesOnPrice($connection, $table, $index, 0,59.96, 85, $count );
        } else if ( $fetch == "PT2") {
            $result = GetGamesOnPrice($connection, $table, $index, 0,29.96, 59.95, $count );
        } else if ( $fetch == "PT1" ) {
            $result = GetGamesOnPrice($connection, $table, $index, 0,0.01, 29.95, $count );
        } else if ( $fetch == "PT0" ) {
            $result = GetGamesOnPrice($connection, $table, $index, 0,0, 0, $count );
        } else if ( $fetch == "GP") {
            $result = GetGamesOnPrice($connection, $table, $index, 1,0, 1000, $count );
        } else if ($fetch == "ALL" ){
            $result = GetGamesOnPrice($connection, $table, $index, 0,0, 1000, $count );
        }

        // Build JSON (Xbox One Game List)
        $xogList = array();
        $xogList['fetch'] = $fetch;
        $xogList['index'] = $index;
        $xogList['xogList'] = $result;

        echo json_encode($xogList, JSON_PRETTY_PRINT);

        //Clear Result
        unset($result);
        $result = null;
        // Close Connection
        $connection->close();
        unset($connetion);



    } else {
        echo "INVALID FETCH REQUEST - FOR FETCH CHOOSE: PT0, PT1, PT2, PT3, GP - FOR COUNT CHOOSE A POSITIVE NUMBER";
    }

}

if (
    isset($_GET['max']) &&
    !isset($_GET['fetch'])
) {
    // Ignore case
    $max = strtoupper($_GET['max']);

    if ( // Validate Price Tier
        $max == "PT0" || // Free
        $max == "PT1" || // $0.01  to $30
        $max == "PT2" || // $30 to $60
        $max == "PT3" || // $60 t $80
        $max == "GP" || // Game Preview
        $max == "ALL" // $0 tp $1000, should get everything
    ) {

        // Establish Connection (MySQL)
        $connection = new mysqli($servername, $username, $password, $database);

        if ($connection->connect_error) {
            die("DATABASE ERROR: " . $connection->connect_error);
        }

        if ( $max == "PT0" ) {
            $priceCeiling = 0;
            $priceFloor = 0;
            $gamePreview = 0;
        } else if ( $max == "PT1" ) {
            $priceCeiling = 29.95;
            $priceFloor = 0.01;
            $gamePreview = 0;
        } else if ( $max == "PT2" ) {
            $priceCeiling = 59.95;
            $priceFloor = 29.96;
            $gamePreview = 0;
        } else if ( $max == "PT3" ) {
            $priceCeiling = 85;
            $priceFloor = 59.96;
            $gamePreview = 0;
        } else if ( $max == "GP" ) {
            $priceCeiling = 1000;
            $priceFloor = 0;
            $gamePreview = 1;
        } else if ( $max == "ALL" ) {
            $priceCeiling = 1000;
            $priceFloor = 0;
            $gamePreview = 0;
        }

        $result = mysqli_query($connection,
            "SELECT * FROM " . $table .
            " WHERE price BETWEEN " . $priceFloor . " AND " . $priceCeiling .
            " AND isGamePreview = " . $gamePreview . ";"
        );

        echo "{ \"max\" : " . $result->num_rows . " }";

    } else {
        echo "INVALID MAX REQUEST: PT0, PT1, PT2, PT3, GP, ALL";
    }

}