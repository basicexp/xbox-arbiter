/**
 * This is a simple Angular Module used to load Xbox One 
 * Game data into an HTML page dynamically
 * 
 * For how to set up an Angular app try: 
 * 
 *      w3schools.com/jquery/default.asp
 * 
 * You need to bind the following $scope variables in an HTML page
 *  
 *      xogListPT0
 *      xogListPT1
 *      xogListPT2
 *      xogListPT3
 * 
 * You also need bind following functions to buttons, passing in the
 * collection you want to update
 * 
 *      getNextXog( <<<xogListPTX>>> )
 *      getPrevXog( <<<xogListPTX>>> )
 * 
 */

var app = angular.module('XboxArbiter', []);
app.controller('XboxArbiterCtrl', function($scope, $http) {

    // Number of games to display for different screen sizes
    $scope.largeGamesToShow = 5;
    $scope.mediumGamesToShow = 3;
    $scope.smallGamesToShow = 1;

    // Screen sizes
    $scope.largeScreen = 992;
    $scope.mediumScreen = 767;

    // Helper function to round to one decimal place
    $scope.roundArbiterScore = function( arbiterScore ) {
        return Math.round(arbiterScore * 10) / 10;
    }

    // Helper function for getting the current screen size
    $scope.screenSize = function() {
        return window.innerWidth;
    }

    // Helper function to decide how many games to show 
    $scope.gamesToShow = function() {
        // Decide how many items to grab based on the screen size
        if ( $scope.screenSize() <= $scope.largeScreen && $scope.screenSize() > $scope.mediumScreen) {
            return $scope.mediumGamesToShow;
        } else if ( $scope.screenSize() <= $scope.mediumScreen) {
            return $scope.smallGamesToShow;
        } else {
            return $scope.largeGamesToShow;
        }
    }

    // Simple constructor
    $scope.XogList = function(fetch, gamesToShow) {
        this.fetch = fetch; // Indicates the Price Tier to fetch
        this.index = -gamesToShow; // Prep for first load
        this.max = -1; // max of games in price tier, set first time you call getNextTenXog
        this.xogList = new Array(); // List of Xbox One Games
    }



    // Initial Values, Setting the price tier for each list
    var gamesToShow = $scope.gamesToShow();
    $scope.xogListPT0 = new $scope.XogList("PT0", gamesToShow);
    $scope.xogListPT1 = new $scope.XogList("PT1", gamesToShow);
    $scope.xogListPT2 = new $scope.XogList("PT2", gamesToShow);
    $scope.xogListPT3 = new $scope.XogList("PT3", gamesToShow);

    // Helper Function for Loading Next Ten Xbox One Games
    $scope.getNextXog = function( xogList ) {

        // If there is not exactly ten games, we are at the end of the list
        // loop back
        if ( xogList.index + $scope.gamesToShow() >= xogList.max  ) {
            xogList.index = -$scope.gamesToShow();
        }

        count = $scope.gamesToShow();

        $http.get("XboxArbiter.php?fetch=" + xogList.fetch + "&index=" + (xogList.index + $scope.gamesToShow()) + "&count=" + count)
            .then(function (response) {


                // If there are items to add, add them
                if ( response.data.xogList.length > 0 ) {

                    xogList.xogList = new Array();
                    xogList.index = response.data.index;

                    for ( var i = 0 ; i < response.data.xogList.length ; i++ ) {
                        response.data.xogList[i].arbiterScore = $scope.roundArbiterScore(response.data.xogList[i].arbiterScore);
                        xogList.xogList.push(response.data.xogList[i]);
                    }

                }

            });

    }

    // Helper Function for Loading Previous Ten Xbox One Games
    $scope.getPrevXog = function( xogList ) {

        // Adjust index to account for overflow
        if ( (xogList.index - $scope.gamesToShow()) < 0 ) {
            // Wrap the list around
            xogList.index = xogList.max - $scope.gamesToShow();
            // Account for the end of the list having fewer than
            // the length of what is shown

        } else { // Progres to previos 
            xogList.index -= $scope.gamesToShow();
        }

        count = $scope.gamesToShow();

        $http.get("XboxArbiter.php?fetch=" + xogList.fetch + "&index=" + xogList.index + "&count=" + count)
            .then(function (response) {
                    // If there are items to add, add them
                    if ( response.data.xogList.length > 0 ) {

                        xogList.xogList = new Array();

                        for ( var i = 0 ; i < response.data.xogList.length ; i++ ) {
                            response.data.xogList[i].arbiterScore = $scope.roundArbiterScore(response.data.xogList[i].arbiterScore);
                            xogList.xogList.push(response.data.xogList[i]);
                        }

                    }

            });
    }

    $scope.getMaximum = function( xogList ) {
        // Check to see if our maximum is set
        if ( xogList.max == -1 ) {
            $http.get("../XboxArbiter.php?max=" + xogList.fetch ).then(
                function( response ) {
                    xogList.max = response.data.max;
                }
            )
        }
    }

    // Initial Load
    $scope.getNextXog($scope.xogListPT3);
    $scope.getNextXog($scope.xogListPT2);
    $scope.getNextXog($scope.xogListPT1);
    $scope.getNextXog($scope.xogListPT0);

    // Set Maximums

    $scope.getMaximum($scope.xogListPT0);
    $scope.getMaximum($scope.xogListPT1);
    $scope.getMaximum($scope.xogListPT2);
    $scope.getMaximum($scope.xogListPT3);

});